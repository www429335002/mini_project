import { defineStore } from 'pinia'

export const useCartStore = defineStore('cart-store', {
  state() {
    return {
      products: []
    }
  },
  actions: {
    addProduct(product, quantity) {
      let productIndex = this.products.findIndex(p => p.product.id === product.id)
      if (productIndex > -1 ) {
        // 商品存在
        this.products[productIndex].quantity += quantity

        if(this.products[productIndex].quantity <= 0) {
          this.products.splice(productIndex, 1)
        }
      } else {
        // 商品不存在
        this.products.push({
          product: product,
          quantity: 1
        })
      }
    }
      
  },
  getters: {
    getTotalPrice() {
      return this.products.reduce((prev, curr) => {
        return prev + curr.product.price * curr.quantity
      }, 0)
    }
  }
})
