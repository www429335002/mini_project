import Vue from 'vue'
import App from './App.vue'

// plugin for pagination
import Paginate from 'vuejs-paginate'
Vue.component('paginate', Paginate)

//引入插件
import vueResource from 'vue-resource'
// 引入Vue-Router
import VueRouter from 'vue-router';
// 引入路由器
import router from './router/index.js'
// 引入Pinia
import { createPinia, PiniaVuePlugin } from 'pinia'

Vue.use(PiniaVuePlugin)
const pinia = createPinia()

// vue-router
Vue.use(VueRouter)
// axios
Vue.use(vueResource)

Vue.config.productionTip = false

import { Button } from 'element-ui';
Vue.component(Button.name, Button);


new Vue({
  render: h => h(App),
  beforeCreate() {
    Vue.prototype.$bus = this
  },
  router, // 使用 router
  pinia, // 使用 pinia
}).$mount('#app')
