/* pagination process */
/* 
  arr: array to be divided
  userPerPage: No. of element in every sub-array
*/
export default function (arr, userPerPage) {
  let resultArr = [];
  let start = 0;
  
  arr.forEach((elemt, index) => {
    let end = (resultArr.length + 1) * userPerPage - 1;

    if (end >= arr.length) {
      end = arr.length - 1;
    }

    if (index === end) {
      let tempArr = arr.slice(start, end + 1);
      resultArr.push(tempArr);
      start = index + 1;
    }
  });

  return resultArr;
}

