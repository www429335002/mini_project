import VueRouter from 'vue-router';

import Home from '@/components/Home';
import Case from '@/components/Case';
import About from '@/components/About';

import TodoList from '@/pages/TodoList/TodoList';
import StickyNotes from '@/pages/StickyNotes/StickyNotes';
import Pagination from '@/pages/Pagination/Pagination'
// import ShoppingCart from '@/pages/ShoppingCart/ShoppingCart'


const router = new VueRouter({
  routes: [
    {
      path: '/home',
      component: Home,
    },
    {
      path: '/case',
      component: Case,
      // 嵌套路由
      children: [
        {
          path: 'todolist',
          component: TodoList,
        },
        {
          path: 'stickynotes',
          component: StickyNotes,
        },
        {
          path: 'pagination',
          component: Pagination
        },
        {
          path: '/shoppingcart',
          component: () => import('@/pages/ShoppingCart/ShoppingCart')
        }
      ],
    },
    {
      path: '/about',
      component: About,
    },
  ],
});

export default router;
