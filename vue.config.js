module.exports = {
  lintOnSave: false, //关闭语法检查
  /* 
    devServer
    解决跨域问题
      http: 协议名 
      localhost: 主机名 
      8080: 端口号 

  */
  devServer: {
    proxy: {
      '/api': {
        target: 'https://api.github.com',
        pathRewrite: { // 注意配置项属性大小写
          '^/api': ''
        }
      }
    }
  }
};
